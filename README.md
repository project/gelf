The GELF module requires the GELF php library.

To install:
Checkout the GELF PHP library from github like so:

    # From your site's docroot.
    git clone https://github.com/Graylog2/gelf-php.git sites/all/libraries/gelf-php.git
